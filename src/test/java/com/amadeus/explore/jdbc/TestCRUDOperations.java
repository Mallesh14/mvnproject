package com.amadeus.explore.jdbc;



import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.amadeus.explore.crud.CrudOperations;
import com.amadeus.explore.crud.Employee;

public class TestCRUDOperations {
	CrudOperations crop = new CrudOperations();
	Employee empObj = new Employee();
	
	@Test
	public void testCreateData(){
		empObj.setId("1");
		empObj.setName("arjun");
		empObj.setSalary("4440");
		crop.createEmployeeData(empObj);
	}

	@Test
	public void testReadData() {
		ArrayList<Employee> arrayList = new ArrayList<Employee>();
		arrayList = crop.readEmpoyeeData("1");
		for(Employee e : arrayList){
			String id = e.getId();
			String name = e.getName();
			String salary = e.getSalary();
			System.out.println(id + "  " + name +"  " + salary);
			
		}
	}
		
	@Test
	public void testUpdateData(){
		
		assertEquals(1,crop.updateEmployeeData("12","20000"));	
	}
	
	@Test
	public void testDeleteData(){
		
	 assertEquals(1,crop.deleteEmployeeData("1"));
	}
	
		
	

}
