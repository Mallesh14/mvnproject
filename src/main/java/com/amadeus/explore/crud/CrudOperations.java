package com.amadeus.explore.crud;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


 public class CrudOperations {
	
	
	 static{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
		
		public static Connection getConnect() throws Exception{
			Connection con = null; 
			try{
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","root");
				return con;
			}catch(Exception e){
				e.printStackTrace();
			}
			return null;	
		}
		public static void close(Connection con){
			try {
				if(con != null)
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public int createEmployeeData(Employee empObj) {
			Connection con = null;
			try{
				con = getConnect();
			
			Statement st = con.createStatement();
			int n = st.executeUpdate("insert into employee values('"+empObj.getId()+"','"+empObj.getName()+"','"+empObj.getSalary()+"')");
			return n ;
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
			close(con);
			}
			return 0 ;
		}
		
		
		public ArrayList<Employee> readEmpoyeeData(String id) {
			 ArrayList<Employee> lt = new ArrayList<Employee>();
			 Employee emp = new Employee();
			Connection con = null ;
			try{
				con =getConnect();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select * from employee where id = '"+ id +"'");
			while(rs.next()){
				emp.setId(rs.getString(1));
				emp.setName(rs.getString(2));
				emp.setSalary(rs.getString(3));
				lt.add(emp);
			}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
			close(con);
			}
			return lt ;
	}
		
		public int updateEmployeeData(String id , String salary) {
			Connection con = null ;
			try{ 
				con = getConnect();
			Statement st = con.createStatement();
			int n = st.executeUpdate("update employee set salary = '"+salary+"' where id = '"+id+"' ");
			return n ;
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				close(con);
			}
			return 0 ;
			}
		
		public int deleteEmployeeData(String id) {
			Connection con = null ;
			try{
				con = getConnect();
			Statement st = con.createStatement();
			int n = st.executeUpdate("delete from employee where id = '"+id+"'");
			return n ;
			}catch(Exception e ){
				e.printStackTrace();
			}finally{
				close(con);
			}
			return 0 ;
			}
	}


